<?php include("header.php");
include("php/UserRepository.php");

$userRepository = new UserRepository();

if (!empty($_GET) && $_GET['action'] === 'delete') {
    $userRepository->delete($_GET['id']);
};
if (!empty($_POST) && ($_POST['userName']) !== null) {
    $userRepository->save($_POST);
}?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <?php include("top_nav.php") ?>
    <?php include("side_nav.php"); ?>
</nav>
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    USERS
                    <small>USERS</small>
                </h1>
                <h3>
                    User names:
                </h3>
                <ul class="list-group">
                    <?php foreach ($userRepository->getAll() as $user): ?>
                        <li class="list-group-item list-group-item-heading">
                            <a href="user.php?action=get&id=<?php echo $user->getId(); ?>">
                                <?php
                                echo $user->getUserName();
                                ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <a class="btn btn-success" href="addEditUser.php"> Add User </a>
            </div>
        </div>
    </div>
</div>
