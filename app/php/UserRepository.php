<?php
include('model/User.php');
include('config/Db.php');

/**
 * Created by PhpStorm.
 * UserRepository: Bartek
 * Date: 30.01.2018
 * Time: 17:43
 */
class UserRepository
{
    private $db;

    private $users;


    public function __construct()
    {
        $this->db = Db::getInstance();
    }

    public function getAll()
    {
        if (!isset($this->userList)) {
            $this->users = $this->db->getDb()->query("SELECT * FROM users")
                ->fetchAll(PDO::FETCH_CLASS, "User");
        }
        return $this->users;
    }

    public function getOneById($id)
    {
        if (!$id) {
            return false;
        }

        $query = $this->db->getDb()->prepare("SELECT * FROM users WHERE id=:id");
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, "User")[0];
    }

    public function delete($id)
    {
        if ($this->getOneById($id) === null) {
            return false;
        }
        $query = $this->db->getDb()->prepare("DELETE FROM users WHERE id = :id");
        $query->bindParam(':id', $id);
        return $query->execute();
    }

    public function save($post)
    {
        if ($this->checkIfExactContactExists($post['userName'])) {
            return false;
        };

        $user = $this->prepareUser($post);

        return $this->db->save('users', $user);
    }


    private function checkIfExactContactExists($userName)
    {
        $query = $this->db->getDb()->
        prepare("SELECT id FROM users WHERE userName=:userName");
        $query->bindParam(':userName', $userName);
        $query->execute();

        if ($query->fetchAll()) {
            return true;
        } else {
            return false;
        }
    }

    public function prepareUser($post)
    {
        $user = new User();
        $user->setId(($post['id']));
        $user->setUserName($post['userName']);
        $user->setFirstName($post['firstName']);
        $user->setPassword($post['password']);
        $user->setLastName($post['lastName']);
        $user->setGender($post['gender']);
        $user->setDob($post['dob']);
        return $user;
    }

    public function edit($id)
    {
    }


}
