CREATE DATABASE IF NOT EXISTS `wsb_users` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci ;
use wsb_users;

CREATE TABLE IF NOT EXISTS `wsb_users`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userName` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `dob` DATE NOT NULL,
  `gender` VARCHAR(45) NOT NULL DEFAULT 'MALE',
  PRIMARY KEY (`id`));


INSERT INTO users (userName,password,firstName,lastName,dob,gender)
VALUES ('rico',123,'John','Doe','1998-04-24','MALE');

INSERT INTO users (userName,password,firstName,lastName,dob,gender)
VALUES ('Bartek',123,'Bartosz','Szymocha','1986-08-19','MALE');