<?php include("header.php");
include("php/UserRepository.php");

$userRepository = new UserRepository();
$edditedUserId = null;
$edditedUser = null;

if (!empty($_GET) && ($_GET['action']) === "edit") {
    $edditedUserId = $_GET['id'];
    $edditedUser = $userRepository->getOneById($edditedUserId);
}?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <?php include("top_nav.php") ?>
    <?php include("side_nav.php"); ?>
</nav>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php $title = $edditedUserId === null ? 'ADD USER' : 'EDIT ' . $edditedUser->getUserName();
                    echo $title ?>
                </h1>
                <form action="users.php" method="post">
                    <input type='hidden' name='id' <?php if ($edditedUser !== null) echo 'value=' . $edditedUserId ?>
                    <div class=' form-group'>
                        <label for='userName'> User Name *:</label>
                        <input type='text' class='form-control' name='userName' id='userName'
                               placeholder='Enter user name' <?php if ($edditedUser !== null) echo 'value=' . $edditedUser->getUserName() ?>
                               required
                    </div>
                        <div class=' form-group'>
                            <label for='password'> Password *:</label>
                            <input type='password' class='form-control' name='password' id='password'
                                   placeholder='Enter password' <?php if ($edditedUser !== null) echo 'value=' . $edditedUser->getPassword() ?>
                                   required
                        </div>

                    <div class="form-group">
                        <label for="firstName">Name*:</label>
                        <input type="text" class="form-control" name="firstName" id="firstName"
                               placeholder="Enter name" <?php if ($edditedUser !== null) echo 'value=' . $edditedUser->getFirstName() ?>
                               required
                    </div>

                    <div class="form-group">
                        <label for="lastName">Surname*:</label>
                        <input type="text" class="form-control" name="lastName" id="lastName"
                               placeholder="Enter surname" <?php if ($edditedUser !== null) echo 'value=' . $edditedUser->getLastName() ?>
                               required
                    </div>

                    <label for="gender">Gender:</label>
                    <select class="form-group" name="gender" id="gender">
                        <option value="MALE"
                        <?php if ($edditedUser !== null && $edditedUser->getGender() == 'MALE') echo 'Selected=selected' ?>
                        ">
                        MALE
                        <option value="FEMALE"
                        <?php if ($edditedUser !== null && $edditedUser->getGender() == 'FEMALE') echo 'Selected=selected' ?>
                        ">
                        FEMALE
                    </select>
                    <div class="form-group">
                        <label for="dob">Date of Birth:</label>
                        <input type="date" class="form-control" name="dob" id="dob"
                            <?php if ($edditedUser !== null) echo 'value=' . $edditedUser->getDOB() ?>
                               required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                    </div>
                    <a class="btn btn-primary" href="users.php"> Cancel </a>
                    <button class="btn btn-success" type="submit">
                        Submit
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

