<?php include("header.php");
include("php/UserRepository.php");

$userRepository = new UserRepository();

if (!empty($_GET) && ($_GET['action']) === "get") {
    $user = $userRepository->getOneById($_GET['id']);
}
?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <?php include("top_nav.php") ?>
    <?php include("side_nav.php"); ?>

</nav>
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php
                    echo $user->getUserName();
                    ?>
                    <br>
                    <small>
                        <?php
                        echo $user;
                        ?>
                    </small>
                </h1>
                <a class="btn btn-default" href="users.php"> Back </a>
                <a class="btn btn-danger" href="users.php?action=delete&id=<?php echo $user->getId(); ?>">Delete</a>
                <a class="btn btn-primary" href="addEditUser.php?action=edit&id=<?php echo $user->getId(); ?>">Edit</a>
            </div>
        </div>
    </div>
</div>
