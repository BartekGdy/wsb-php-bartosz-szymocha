<?php
ob_start();
include("php/config/Config.php");
include("php/config/Db.php");

include("header.php"); ?>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <?php include("top_nav.php") ?>
            <?php include("side_nav.php"); ?>
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome
                            <small>PHP Class Project Bartosz Szymocha</small>
                        </h1>
                        <h3 class="panel-body">
                            Basic CRUD for User model <br>
                            Please navigate to User side bar
                        </h3>
                        <h5>
                            Please run sql script in sql folder to create database and table
                        </h5>
                    </div>
                </div>
            </div>
        </div>